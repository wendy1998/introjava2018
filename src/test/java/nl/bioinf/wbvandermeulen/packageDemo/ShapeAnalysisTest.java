/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.packageDemo;

import org.junit.Test;

import static org.junit.Assert.*;

public class ShapeAnalysisTest {

    @Test
    public void getSurfaceAreaOfCircle() {
        System.out.println("start test");
        double input = 3.0;
        double expected = Math.PI * (input * input);
        double observed = ShapeAnalysis.getSurfaceAreaOfCircle(input);
        assertEquals(expected, observed, 0.0001);

        input = 0.0;
        expected = Math.PI * (input * input);
        observed = ShapeAnalysis.getSurfaceAreaOfCircle(input);
        assertEquals(expected, observed, 0.0001);

        System.out.println("end test");
    }
}