/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.packageDemo;

public class ShapeAnalysis {
    public static double getSurfaceAreaOfCircle(double radius) {
        return Math.PI * Math.pow(radius, 2);
    }
}
