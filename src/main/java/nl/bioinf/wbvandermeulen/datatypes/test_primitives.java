/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.datatypes;

public class test_primitives {
    static int counter;
    public static void main(String[] args) {
        byte b = 0;
        for (int x = 0; x <= 300; x++) {
            System.out.println("b = " + b);
            b++;
        }


        int counter2;

        System.out.println("counter = " + counter);

        counter2 = 9;
        System.out.println("counter2 = " + counter2);

        char n = 'n';
        long l = n;

        System.out.println("l = " + l);
        
        n++;
        System.out.println("n = " + n);
    }
}
