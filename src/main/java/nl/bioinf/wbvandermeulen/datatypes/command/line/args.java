/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.datatypes.command.line;

public class args {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.println("arg = " + args[i]);
        }
    }
}
