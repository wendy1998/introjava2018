/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.hello;

import java.util.Objects;

public class horse implements Comparable<horse> {
    public int id;
    public String name;
    public int weightInKilograms;
    public String color;

    void gallop(double speedInKmPerHour){
        System.out.println("Horse Galloping at " + speedInKmPerHour + " Km per Hour");
    }

    @Override
    public String toString() {
        return "Horse: {color = " + color + ", weight = " + weightInKilograms + ", name = " + name + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        horse horse = (horse) o;
        return weightInKilograms == horse.weightInKilograms &&
                Objects.equals(color, horse.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weightInKilograms, color);
    }

    @Override
    public int compareTo(horse o) {
        // zero if equel
        // <0 if this < other
        //>0 if this > other
        // return this.weightInKilograms - o.weightInKilograms;

        return Integer.compare(this.weightInKilograms, o.weightInKilograms);  //delegation
    }
}
