/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */
package nl.bioinf.wbvandermeulen.hello;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World");
        horse horse1 = new horse();
        horse1.gallop(66);
        horse1.color = "brown";
        horse1.weightInKilograms = 612;
        System.out.println("horse1.weightInKilograms = " + horse1.weightInKilograms);
        System.out.println("Bye now");
    }
}