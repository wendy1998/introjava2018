/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.exceptions;

public class ExceptionsDemo {
    public static void main(String[] args) {
        foo();
        System.out.println("doei");
    }

    private static void foo() {
        try {
            bar();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("hallo");
    }

    private static int bar() {
        System.out.println("moi");
//        try {
          return 42 / 0;
//        } catch (ArithmeticException ex) {
//            System.out.println("caught an exception!");
//        }
    }
}
