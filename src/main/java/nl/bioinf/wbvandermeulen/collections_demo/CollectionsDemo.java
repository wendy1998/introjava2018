/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.collections_demo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import nl.bioinf.wbvandermeulen.hello.horse;

public class CollectionsDemo {
    public static void main(String[] args) {
        ArrayList<horse> horses = createHorseList();
//        System.out.println("horse1 = " + horses.get(1));
        System.out.println("horses = " + horses);

//        HashSet<horse> horseSet = new HashSet<>();
//        horseSet.addAll(horses);

        Collections.sort(horses);

        System.out.println("horses = " + horses);

    }
    public static ArrayList<horse> createHorseList() {
        horse horse1 = new horse();
        horse1.color = "brown";
        horse1.weightInKilograms = 644;
        horse1.name = "FastMF";

        horse horse2 = new horse();
        horse2.color = "white";
        horse2.weightInKilograms = 732;
        horse2.name = "Tinkerbell";

        horse horse3 = new horse();
        horse3.color = "brown";
        horse3.weightInKilograms = 644;
        horse3.name = "FasterMF";

        horse horse4 = new horse();
        horse4.color = "white";
        horse4.weightInKilograms = 644;
        horse4.name = "DavidBowie";

        horse horse5 = new horse();
        horse5.color = "brown";
        horse5.weightInKilograms = 801;
        horse5.name = "EffectOfLife";


        ArrayList<horse> horses = new ArrayList<>();
        horses.add(horse1);
        horses.add(horse2);
        horses.add(horse3);
        horses.add(horse4);
        horses.add(horse5);

        return horses;
    }
}
