/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.testtube;

/**
 * Class Cell simulates a growing cell
 */
public class Cell {
    private int size;
    void grow() {
        System.out.println("Growing bigger...");
    }

    public int getSize() {
        return size;
    }

    public void setSize(int newSize) {
        if (newSize <= 0 || newSize > 25) {
            throw new IllegalArgumentException("Illegal cell size attempted: " + newSize);
        }
        this.size = newSize;
    }
}
