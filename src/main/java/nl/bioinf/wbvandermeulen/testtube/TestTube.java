/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.testtube;

public class TestTube {
    public static void main(String[] args) {
        System.out.println("Starting Simulation...");
        TestTube tube = new TestTube();
        tube.startSimulation();
    }

    private void startSimulation() {
        System.out.println("Cells are growing...");
        Cell cell = new Cell();
        cell.grow();

        System.out.println("cell.size = " + cell.getSize());
        
        cell.setSize(14);

        System.out.println("cell.size = " + cell.getSize());
    }
}
