/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.sorting;

import nl.bioinf.wbvandermeulen.hello.horse;
import java.util.Comparator;

public class FurComparator implements Comparator<horse> {
    @Override
    public int compare(horse o1, horse o2) {
        //delegate to class String
        int comp = o1.color.compareTo(o2.color);

        if (comp == 0) {
            return Integer.compare(o1.weightInKilograms, o2.weightInKilograms);
        } else {
            return comp;
        }
    }
}
