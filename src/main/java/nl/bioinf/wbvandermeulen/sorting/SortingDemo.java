/*
 * Copyright (c) 2018 Wendy van der Meulen.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wbvandermeulen.sorting;

import nl.bioinf.wbvandermeulen.collections_demo.CollectionsDemo;
import nl.bioinf.wbvandermeulen.hello.horse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SortingDemo {
    public static void main(String[] args) {
        ArrayList<horse> horses = CollectionsDemo.createHorseList();

        System.out.println("horses = " + horses);

//        Collections.sort(horses);
//
//        System.out.println("horses = " + horses);
        
        Collections.sort(horses, new FurComparator());

        System.out.println("horses = " + horses);

        Collections.sort(horses, new Comparator<horse>() {
            @Override
            public int compare(horse o1, horse o2) {
                return o1.name.compareTo(o2.name);
            }
        });

    }
}
